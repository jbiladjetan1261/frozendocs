#to do list for bzflag
---------------------

* encapsulate stuff requiring platform #ifdef's:
   - networking API into libNet.a.
   - fork/exec (used in menus.cxx) into libPlatform.a
   - file system stuff (dir delimiter, etc.)
   - user name stuff

1.* write bzfbiff
   watch server for players;  run program when somebody joins

2.* windows makefiles
   currently have microsoft visual c++ projects only

3. * auto-generated dependency support in makefiles
   manual dependencies cause too much trouble

* smarter **robots**

* add type of shot (normal, gm, sw, etc) [to killed message](http:/google.com)

* support resolutions besides 640x480 on passthrough *3Dfx cards*



